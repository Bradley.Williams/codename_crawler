using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.Pool;

public class RoomSpawner : MonoBehaviour {
    /* 
        What needs to be done in this script?
        Give a width and height for area of the rooms to spawn in 
        Have a max number of rooms that can spawn
        Have a prefab object that can spawn in as a room
        have the rooms connect via hallways

        Process:
        Firstly get some rooms to spawn (Done)
        then limit them to an area (Done)
        then limit the number that spawn (Done)
        then have them check if they are intersecting with a room and remove one or the other. (Done)
        then have the rooms generate with entrances and exits. (Done)
        then have the game determine the nearest points from each other. (Done (But found a flaw))
        have the game path find between the points instantiating hallways. (Working on)

        A* Logic
        G cost is how far the grid node is from the starting node.
        H cost is how far the grid node is from the destination node.
        F cost = G cost + H cost

        Looks at all grid nodes and chooses the one with the lowest F cost.
        If nodes have the same F cost the algorithm looks for the lowest H cost.

        My algorithm wont take diagonals into account.
     */

    //  Jared please ignore attempt 1 it was the first thing that came to my mind and didn't work well it is commented out so that I can
    //  Still refer to it if need be but so far haven't needed to.

    // Have a list that contains the rooms that have been checked for nearest and check if it it contains that object so you can then just
    // Daisy chain the rooms together

    // TODO : Corrodior(Breadth First search)
    // 

    #region Attempt 1, spawning and turning off areas
    // /* 
    //    Rather than instantiate get from object pool and set scale.
    //  */

    // public static RoomSpawner Instance;

    // [SerializeField] private GameObject _roomPrefab;

    // private List<GameObject> _roomList = new List<GameObject> ();
    // private List<GameObject> _spawnedRoomsToStayList = new List<GameObject> ();

    // public ObjectPool<GameObject> _roomPool;

    // [Header ("Room Settings")]
    // [SerializeField] int _minNumRoomsPerFloor, _maxNumRoomsPerFloor;
    // [SerializeField] int _numFloorsCurrent, _numFloors;
    // [SerializeField] int _heightDiffForFloors = 5;
    // [SerializeField] public int _widthCanSpawn, _heightCanSpawn;

    // [HideInInspector]
    // public static int _currenNumRooms;

    // [HideInInspector]
    // public Vector3 _randomV3Pos, _randomV3RoomScale;
    // int _roomYSpawnHeight = 0;

    // // private void Awake () {
    // //     _numFloorsCurrent = 0;
    // //     _currenNumRooms = 0;
    // //     GenerateObjectPool ();
    // //     SpawnRooms ();
    // // }

    // private void GenerateObjectPool () {
    //     _roomPool = new ObjectPool<GameObject> (createFunc: () => {
    //             return Instantiate (original: _roomPrefab);
    //         }, actionOnGet : Room => {
    //             Room.SetActive (true);
    //         }, actionOnRelease : Room => {
    //             Room.SetActive (false);
    //         }, Room => {
    //             Destroy (Room.gameObject);
    //         }, true,
    //         _minNumRoomsPerFloor,
    //         _maxNumRoomsPerFloor * _numFloors);
    // }

    // void SpawnRooms () {
    //     // _numFloorsCurrent++;
    //     for (int i = 0; i < _widthCanSpawn; i++) {
    //         for (int j = 0; j < _heightCanSpawn; j++) {
    //             if (_currenNumRooms < _maxNumRoomsPerFloor) {
    //                 // Spawns room
    //                 GameObject _room = _roomPool.Get ();
    //                 // Randomizes size and position of room
    //                 RandomizeRooms (_room);
    //                 // Random Pos in the world
    //                 _room.transform.position = _randomV3Pos;
    //                 _roomList.Add (_room.gameObject);
    //                 _currenNumRooms++;
    //             }
    //         }
    //     }
    //     CheckOverlap ();
    // }

    // void RandomizeRooms (GameObject room) {
    //     Room _room = room.GetComponent<Room> ();

    //     _randomV3Pos = new Vector3 (
    //         Random.Range (minInclusive: -_widthCanSpawn, _widthCanSpawn),
    //         _roomYSpawnHeight,
    //         Random.Range (-_heightCanSpawn, _heightCanSpawn));
    //     _randomV3RoomScale = new Vector3 (
    //         Random.Range (minInclusive: _room._roomMinWidth, _room._roomMaxWidth),
    //         1,
    //         Random.Range (minInclusive: _room._roomMinHeight, _room._roomMaxHeight));
    //     _roomPrefab.transform.localScale = _randomV3RoomScale;
    // }

    // void CheckOverlap () {
    //     for (int i = 0; i < _roomPool.CountAll; i++) {
    //         CheckIfOverlapping (_roomList[i]);
    //     }
    // }

    // void CheckIfOverlapping (GameObject _roomBeingChecked) {
    //     // Checks each room and sees if something is overlapping it
    //     BoxCollider _roomCollider = _roomBeingChecked.GetComponent<BoxCollider> ();

    //     // Debug.Log (_roomCollider.size);

    //     // This sets the gameObject's visibility to false
    //     // _roomPool.Release (_roomBeingChecked);

    //     // Debug.Log (_roomBeingChecked.transform.position.x / 2 + "," + _roomBeingChecked.transform.position.y / 2 + "," + _roomBeingChecked.transform.position.z / 2);
    //     Vector3 _roomCenter = new Vector3 (
    //         _roomBeingChecked.transform.position.x / 2,
    //         _roomBeingChecked.transform.position.y / 2,
    //         _roomBeingChecked.transform.position.z / 2
    //     );
    //     // Debug.Log (_roomCenter);

    //     // Getting the bounds in order to check the box
    //     Vector3 _roomBounds = new Vector3 () {
    //         x = _roomBeingChecked.transform.lossyScale.x,
    //         y = _roomBeingChecked.transform.lossyScale.y,
    //         z = _roomBeingChecked.transform.lossyScale.z
    //     };

    //     if (_roomBounds.x < 0) {
    //         _roomBounds.x *= -1;
    //     }

    //     if (_roomBounds.y < 0) {
    //         _roomBounds.y *= -1;
    //     }

    //     if (_roomBounds.z < 0) {
    //         _roomBounds.z *= -1;
    //     }

    //     // Debug.Log (_roomBounds);

    //     if (Physics.CheckBox (_roomCenter, _roomBounds)) {
    //         Debug.Log ("Touching");
    //         if (_roomBeingChecked.activeInHierarchy) {
    //             _roomPool.Release (_roomBeingChecked);
    //         }
    //     }

    //     if (_roomPool.CountActive < _minNumRoomsPerFloor) {
    //         _currenNumRooms = _roomPool.CountActive;
    //         SpawnRooms ();
    //     }
    // }

    // private void OnDisable () {
    //     _roomList.Clear ();
    // }
    #endregion

    [SerializeField] List<GameObject> _listOfRoomPrefabs = new List<GameObject> ();
    [HideInInspector]
    [SerializeField] List<GameObject> _listOfRoomsToCheckNearest = new List<GameObject> ();
    [HideInInspector]
    [SerializeField] List<GameObject> _listOfRoomsSpawned = new List<GameObject> ();
    [SerializeField] float _spawnRadius = 25;
    public Vector2 _regionSize;
    [SerializeField] int _numTimesTest = 30;

    List<Vector2> _spawnPoints;

    [Header ("Room Settings")]
    [SerializeField] int _numberOfRoomsFloor;
    public static int _numRoomsPerFloor;
    [SerializeField] int _numFloors;

    private int _numRoomsThisFloor = 0;

    private int _floor;
    [SerializeField] int _spaceBetweenFloors;

    private void Awake () {
        _numRoomsPerFloor = _numberOfRoomsFloor;
        _floor = 0;
        InstantiateRoomArea ();
    }

    private void InstantiateRoomArea () {
        _spawnPoints = PoissonPoints.GeneratePoints (_spawnRadius, _regionSize, _numTimesTest);
        SpawnRooms ();
    }

    void SpawnRooms () {
        if (_spawnPoints != null) {
            GameObject _floorParent = new GameObject ("floor " + _floor);
            foreach (Vector2 point in _spawnPoints) {
                if (_numRoomsThisFloor < _numRoomsPerFloor) {
                    // Creates the room casting the pos to an int makes it easier to path find not having to work with decimals.
                    GameObject _spawnedRoom = Instantiate (_listOfRoomPrefabs[Random.Range (0, _listOfRoomPrefabs.Count)], new Vector3 ((int) point.x, _floor * _spaceBetweenFloors, (int) point.y), Quaternion.identity);
                    _spawnedRoom.transform.SetParent (_floorParent.transform, false);
                    _numRoomsThisFloor++;
                    _listOfRoomsToCheckNearest.Add (_spawnedRoom);
                    _listOfRoomsSpawned.Add (_spawnedRoom);
                }
            }
            _floor++;
            FindNearestRoom ();
        }

        if (_floor < _numFloors) {
            _numRoomsThisFloor = 0;
            _listOfRoomsToCheckNearest.Clear ();
            _listOfRoomsSpawned.Clear ();
            InstantiateRoomArea ();
        }
    }

    private void FindNearestRoom () {
        foreach (GameObject room in _listOfRoomsToCheckNearest) {
            Room _roomBeingChecked = room.GetComponent<Room> ();
            GameObject _closestRoom;

            // This check is due to the fact that the first element in the list would always say that the closest room was itself.
            if (room == _listOfRoomsToCheckNearest[0]) {
                _closestRoom = _listOfRoomsToCheckNearest[0 + 1];
            } else {
                _closestRoom = _listOfRoomsToCheckNearest[0];
            }

            // Looping through entire list of spawned rooms in order to find the closest room.
            for (int i = 0; i < _listOfRoomsSpawned.Count - 1; i++) {
                float currentClostestDist = Vector3.Distance (_closestRoom.transform.position, room.transform.position);
                float checkedDistance = Vector3.Distance (_listOfRoomsSpawned[i].transform.position, room.transform.position);

                // If it's not itself as it would be the closest to itself
                if (_listOfRoomsToCheckNearest[i] != room) {
                    if ((checkedDistance < currentClostestDist)) {
                        _closestRoom = _listOfRoomsSpawned[i];
                    }
                }
            }
            // Once the closest room is found it sets that rooms closet room variable to that game object.
            _roomBeingChecked._nearestRooms.Add (_closestRoom);
        }
    }

}

// Credit to Sabastian Lague on how to do PoissonPoints
public static class PoissonPoints {
    public static List<Vector2> GeneratePoints (float radiusToCheckPoints, Vector2 spawnRegionSize, int numTimesSpawnCheck = 30) {
        // This gives back the size of the area a single object can spawn in
        float singleSpawnArea = radiusToCheckPoints / Mathf.Sqrt (2);

        // this 2D array stores the size of the grid that objects can spawn in
        int[, ] gridToSpawn = new int[Mathf.CeilToInt (spawnRegionSize.x / singleSpawnArea),
            Mathf.CeilToInt (spawnRegionSize.y / singleSpawnArea)];

        // points holds the possible points an object can spawn, spawnpoints has a point that then is checked
        // to see if other points can spawn around it
        List<Vector2> _points = new List<Vector2> ();
        List<Vector2> _possibleSpawnPoints = new List<Vector2> ();

        // Gives a spawn point to start off the spawning
        _possibleSpawnPoints.Add (spawnRegionSize / 2);
        // Loops points
        while (_possibleSpawnPoints.Count > 0) {
            int spawnIndex = Random.Range (0, _possibleSpawnPoints.Count);
            bool _pointAccepted = false;

            Vector2 spawnCentre = _possibleSpawnPoints[spawnIndex];

            // loops through to see if a point can be spawned
            for (int i = 0; i < numTimesSpawnCheck; i++) {
                float angle = Random.value * Mathf.PI * 2;
                Vector2 dir = new Vector2 (Mathf.Sin (angle), Mathf.Cos (angle));
                // This is the point that gets checked to see if it is a valid area
                Vector2 checkedPoint = spawnCentre + dir * Random.Range ((int) radiusToCheckPoints, (int) radiusToCheckPoints * 2);
                if (IsValid (checkedPoint, spawnRegionSize, singleSpawnArea, radiusToCheckPoints, _points, gridToSpawn) && _points.Count < RoomSpawner._numRoomsPerFloor) {
                    _points.Add (checkedPoint);
                    _possibleSpawnPoints.Add (checkedPoint);
                    gridToSpawn[(int) (checkedPoint.x / singleSpawnArea), (int) (checkedPoint.y / singleSpawnArea)] = _points.Count;
                    _pointAccepted = true;
                    break;
                }
            }
            if (!_pointAccepted) {
                _possibleSpawnPoints.RemoveAt (spawnIndex);
            }
        }
        return _points;
    }

    // Checks if the point is a valid area to spawn
    static bool IsValid (Vector2 _checkedPoint, Vector2 _gridSize, float singleSpawnAreaSize, float radius, List<Vector2> points, int[, ] spawnAreaSize) {
        if (_checkedPoint.x >= 0 && _checkedPoint.x < _gridSize.x && _checkedPoint.y >= 0 && _checkedPoint.y < _gridSize.y) {
            int _cellX = (int) (_checkedPoint.x / singleSpawnAreaSize);
            int _cellY = (int) (_checkedPoint.y / singleSpawnAreaSize);
            int _searchStartX = Mathf.Max (0, _cellX - 2);
            int _searchEndX = Mathf.Min (_cellX + 2, spawnAreaSize.GetLength (0) - 1);
            int _searchStartY = Mathf.Max (0, _cellY - 2);
            int _searchEndY = Mathf.Min (_cellY + 2, spawnAreaSize.GetLength (1) - 1);

            for (int x = _searchStartX; x <= _searchEndX; x++) {
                for (int y = _searchStartY; y <= _searchEndY; y++) {
                    int pointIndex = spawnAreaSize[x, y] - 1;
                    if (pointIndex != -1) {
                        float sqrDist = (_checkedPoint - points[pointIndex]).sqrMagnitude;
                        if (sqrDist < radius * radius) {
                            return false;
                        }
                    }
                }
            }
            return true;
        }
        return false;
    }
}