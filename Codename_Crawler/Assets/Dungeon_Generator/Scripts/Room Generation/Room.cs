using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Room : MonoBehaviour {

    // May use room doors as a way to access the point in which the A* should start from and use another rooms other door as the destination.
    public List<GameObject> _roomDoors;

    public List<GameObject> _nearestRooms;
}