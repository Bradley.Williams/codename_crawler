using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public class WallGenerator : MonoBehaviour {

    [SerializeField] GameObject _wall;
    [SerializeField] Vector3 _offSet;

    private GameObject _spawnedWall;
    private void Start () {
        _spawnedWall = Instantiate (_wall, transform.position + _offSet, this.gameObject.transform.rotation);
        _spawnedWall.transform.SetParent (this.transform);
    }

    private void OnTriggerEnter (Collider other) {
        if (other != null) {
            Destroy (_spawnedWall);
        }
    }
}