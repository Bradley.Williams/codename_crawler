using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Node {
    public bool walkable;
    public Vector3 nodePos;

    public int gridX;
    public int gridY;

    public int gCost;
    public int hCost;

    public Node _parent;

    public Node (bool _isWalkable, Vector3 _NodePosition, int _gridXPos, int _gridYPos) {
        walkable = _isWalkable;
        nodePos = _NodePosition;
        gridX = _gridXPos;
        gridY = _gridYPos;
    }

    // Never need to set f Cost which is why there is no setter
    // This is due to the fact that you only ever get fCost
    public int fCost {
        get {
            return gCost + hCost;
        }
    }
}