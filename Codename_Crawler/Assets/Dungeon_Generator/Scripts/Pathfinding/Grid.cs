using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public class Grid : MonoBehaviour {

    public static Grid Instance;

    [Header ("Grid Settings")]
    [SerializeField] LayerMask unwalkableMask;
    [SerializeField] Vector2 gridWorldSize;
    [SerializeField] float nodeRadius;

    [Header ("Debug Settings")]
    [SerializeField] bool _showGrid;

    [SerializeField] GameObject _hallwayPrefab;
    [SerializeField] GameObject _objectParent;

    public Transform seeker, target;

    Node[, ] grid;

    float nodeDiameter;
    int gridsizeX, gridSizeY;
    RoomSpawner _roomDetails;

    /* 
        Set the position of the boundary of the grid in order for it to line up with the spawned poisson points.
     */
    private void Awake () {
        Instance = this;
        _roomDetails = GetComponent<RoomSpawner> ();
        // gridWorldSize.x = _roomDetails._regionSize.x;
        // gridWorldSize.y = _roomDetails._regionSize.y;
        _objectParent.transform.position = new Vector3 (gridWorldSize.x / 2, 0, gridWorldSize.y / 2);
        this.transform.SetParent (_objectParent.transform, false);
    }

    // Round the grid size to an int as it is easier to work with.
    private void Start () {
        nodeDiameter = nodeRadius * 2;
        gridsizeX = Mathf.RoundToInt (gridWorldSize.x / nodeDiameter);
        gridSizeY = Mathf.RoundToInt (gridWorldSize.y / nodeDiameter);
        CreateGrid ();
    }

    public List<Node> path;

    /* 
        This section allows you to visualise the nodes that are created in the 2D array below
     */
    private void OnDrawGizmos () {
        Gizmos.DrawWireCube (transform.position, new Vector3 (gridWorldSize.x, 1, gridWorldSize.y));

        if (grid != null) {
            foreach (Node n in grid) {
                Gizmos.color = (n.walkable) ? Color.white : Color.red;
                if (path != null) {
                    if (path.Contains (n)) {
                        Gizmos.color = Color.black;
                    }
                }
                Gizmos.DrawCube (n.nodePos, Vector3.one * (nodeDiameter - 0.1f));
            }
        }
    }

    /* 
        This section creates a new 2D array of nodes cycling through the entire grid area defining each point as a node
     */
    void CreateGrid () {
        grid = new Node[gridsizeX, gridSizeY];
        Vector3 worldBottomLeft = transform.position - Vector3.right * gridWorldSize.x / 2 - Vector3.forward * gridWorldSize.y / 2;

        for (int i = 0; i < gridsizeX; i++) {
            for (int j = 0; j < gridSizeY; j++) {
                Vector3 worldPoint = worldBottomLeft + Vector3.right * (i * nodeDiameter + nodeRadius) + Vector3.forward * (j * nodeDiameter + nodeRadius);
                bool walkable = !(Physics.CheckSphere (worldPoint, nodeRadius, unwalkableMask));

                grid[i, j] = new Node (walkable, worldPoint, i, j);
            }
        }
    }

    /* 
        This section allows us to get the world position of a single grid point in the world which would allow us to detect which node the
        path finder is on

        The pathfinder is yet to be created.
     */
    public Node NodeFromWorldPoint (Vector3 worldpos) {
        float _percentX = (worldpos.x + gridWorldSize.x / 2) / gridWorldSize.x;
        float _percentY = (worldpos.z + gridWorldSize.y / 2) / gridWorldSize.y;
        _percentX = Mathf.Clamp01 (_percentX);
        _percentY = Mathf.Clamp01 (_percentY);

        int x = Mathf.RoundToInt ((gridsizeX - 1) * _percentX);
        int y = Mathf.RoundToInt ((gridSizeY - 1) * _percentY);

        return grid[x, y];
    }

    // This method is used to get the neighbouring nodes for a single node
    public List<Node> GetNodeNeighbours (Node node) {
        List<Node> neighbours = new List<Node> ();
        // TODO: Change loops to be individual so that it does not include diagonals.

        for (int gridX = -1; gridX <= 1; gridX++) {
            for (int gridZ = -1; gridZ <= 1; gridZ++) {
                if (gridX == 0 && gridZ == 0 ||
                    gridX == -1 && gridZ == 1 ||
                    gridX == 1 && gridZ == 1 ||
                    gridX == -1 && gridZ == -1 ||
                    gridX == 1 && gridZ == -1) {
                    continue;
                }

                int checkX = node.gridX + gridX;
                int checkZ = node.gridY + gridZ;

                if (checkX >= 0 && checkX < gridsizeX &&
                    checkZ >= 0 && checkZ < gridSizeY) {
                    neighbours.Add (grid[checkX, checkZ]);
                }
            }
        }
        return neighbours;
    }

    public void CreatePath () {
        if (grid != null) {
            foreach (Node n in grid) {
                if (path != null) {
                    if (path.Contains (n)) {
                        GameObject Floor = Instantiate (_hallwayPrefab, n.nodePos, Quaternion.identity);
                    }
                }
            }
        }
    }

    #region BFS Tutorial Code Not Used

    /* 
        Using Breadth First Search Algorithm in order to find shortest
        path to generate the hallways
     */
    // public Vector3 FindShortestPathBFS (Vector3 startPos, Vector3 endPos) {
    //     int nodesVisitedCount = 0;

    //     Queue<Vector3> queue = new Queue<Vector3> ();
    //     HashSet<Vector3> exploredNodes = new HashSet<Vector3> ();
    //     queue.Enqueue (startPos);

    //     while (queue.Count != 0) {
    //         Vector3 currentNode = queue.Dequeue ();
    //         nodesVisitedCount++;

    //         if (currentNode == endPos) {
    //             Debug.Log ("Found End Goal at: " + endPos);

    //             return currentNode;
    //         }

    //         IList<Vector3> nodes = GetWalkableNodes (currentNode);

    //         foreach (Vector3 node in nodes) {
    //             if (!exploredNodes.Contains (node)) {
    //                 // Adding this to explored nodes as it has been explored
    //                 exploredNodes.Add (node);

    //                 // Store reference to previous node
    //                 nodeParent.Add (node, currentNode);

    //                 // Add the node to the queue to be examined
    //                 queue.Enqueue (node);
    //             }
    //         }
    //     }
    //     return startPos;
    // }

    // bool CanMove (Vector3 nextPos) {
    //     return (walkablePos.ContainsKey (nextPos) ? walkablePos[nextPos] : false);
    // }

    // IList<Vector3> GetWalkableNodes (Vector3 currentNodeChecked) {
    //     IList<Vector3> walkableNodes = new List<Vector3> ();

    //     IList<Vector3> possibleNodes = new List<Vector3> () {
    //         new Vector3 (currentNodeChecked.x + 1, currentNodeChecked.y, currentNodeChecked.z),
    //         new Vector3 (currentNodeChecked.x - 1, currentNodeChecked.y, currentNodeChecked.z),
    //         new Vector3 (currentNodeChecked.x, currentNodeChecked.y, currentNodeChecked.z + 1),
    //         new Vector3 (currentNodeChecked.x, currentNodeChecked.y, currentNodeChecked.z - 1),
    //         new Vector3 (currentNodeChecked.x + 1, currentNodeChecked.y, currentNodeChecked.z + 1),
    //         new Vector3 (currentNodeChecked.x + 1, currentNodeChecked.y, currentNodeChecked.z - 1),
    //         new Vector3 (currentNodeChecked.x - 1, currentNodeChecked.y, currentNodeChecked.z + 1),
    //         new Vector3 (currentNodeChecked.x - 1, currentNodeChecked.y, currentNodeChecked.z - 1)
    //     };

    //     foreach (Vector3 node in possibleNodes) {
    //         if (CanMove (node)) {
    //             walkableNodes.Add (node);
    //         }
    //     }

    //     return walkableNodes;
    // }

    #endregion
}