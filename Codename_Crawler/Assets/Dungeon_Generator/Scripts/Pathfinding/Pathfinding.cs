using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Pathfinding : MonoBehaviour {

    public static Pathfinding Instance;

    Grid grid;

    private void Awake () {
        if (Instance != null) {
            Destroy (this);
        } else {
            Instance = this;
        }
        grid = GetComponent<Grid> ();
    }

    private void Start () {
        FindPath (Grid.Instance.seeker.localPosition, Grid.Instance.target.localPosition);
    }

    public void FindPath (Vector3 startPos, Vector3 endPos) {
        Node startNode = grid.NodeFromWorldPoint (startPos);
        Node endNode = grid.NodeFromWorldPoint (endPos);

        // This is the list that will take a node and check the other nodes around it
        List<Node> nodesToCheck = new List<Node> ();
        // This is the set that has already been checked and moved along from.
        HashSet<Node> nodesChecked = new HashSet<Node> ();

        nodesToCheck.Add (startNode);

        while (nodesToCheck.Count > 0) {
            Node currentNode = nodesToCheck[0];
            for (int i = 1; i < nodesToCheck.Count; i++) {
                if (nodesToCheck[i].fCost < currentNode.fCost ||
                    nodesToCheck[i].fCost == currentNode.fCost &&
                    nodesToCheck[i].hCost < currentNode.hCost) {
                    currentNode = nodesToCheck[i];
                }
            }

            nodesToCheck.Remove (currentNode);
            nodesChecked.Add (currentNode);

            if (currentNode == endNode) {
                RetracePath (startNode, endNode);
                grid.CreatePath ();
                return;
            }

            foreach (Node neighbouringNode in grid.GetNodeNeighbours (currentNode)) {
                if (!neighbouringNode.walkable || nodesChecked.Contains (neighbouringNode)) {
                    continue;
                }

                int newMovementCost = currentNode.gCost + GetDistBetweenNodes (currentNode, neighbouringNode);
                if (newMovementCost < neighbouringNode.gCost ||
                    !nodesToCheck.Contains (neighbouringNode)) {
                    neighbouringNode.gCost = newMovementCost;
                    neighbouringNode.hCost = GetDistBetweenNodes (neighbouringNode, endNode);
                    neighbouringNode._parent = currentNode;

                    if (!nodesToCheck.Contains (neighbouringNode)) {
                        nodesToCheck.Add (neighbouringNode);
                    }
                }
            }
        }
    }

    void RetracePath (Node startNode, Node endNode) {
        List<Node> path = new List<Node> ();
        Node currentNode = endNode;

        while (currentNode != startNode) {
            path.Add (currentNode);
            currentNode = currentNode._parent;
        }

        path.Reverse ();

        grid.path = path;
    }

    int GetDistBetweenNodes (Node _nodeA, Node _nodeB) {
        int dstX = Mathf.Abs (_nodeA.gridX - _nodeB.gridX);
        int dstY = Mathf.Abs (_nodeA.gridY - _nodeB.gridY);

        // Formula is because looking it as an y axis and x axis the equation 
        // uses 10 * dst Y as I am trying to not go diagonal still testing diagonal would be 14 as
        // a diagonal move is a 1.4 cost x 10 to get whole numbers
        if (dstX > dstY) {
            return 14 * dstY + 10 * (dstX - dstY);
        }

        return 14 * dstX + 10 * (dstY - dstX);
    }
}