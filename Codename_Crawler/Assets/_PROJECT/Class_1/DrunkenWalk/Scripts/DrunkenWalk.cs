using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class DrunkenWalk : MonoBehaviour {
    [SerializeField] RawImage _generatedOutput;
    [SerializeField] Color32 _backgroundColour, _lineColour;
    [SerializeField, Range (0.01f, 1f)] float _resolutionScale = 1f;
    [SerializeField] int _paddingPixel = 2;
    [SerializeField] int _pixelsPerFrame = 2;

    bool _isGenerating = false;

    public void Generate () {
        _isGenerating = !_isGenerating;
        if (_isGenerating) {
            Debug.Log ($"Start Generating");
            StartCoroutine (Walking ());
        } else {
            Debug.Log ($"Stop Generating");
        }
    }

    IEnumerator Walking () {
        Texture2D texture = new Texture2D ((int) (Screen.width * _resolutionScale), (int) (Screen.height * _resolutionScale), TextureFormat.RGBA32, true);
        texture.filterMode = FilterMode.Point;
        Vector2Int point = new Vector2Int (texture.width / 2, texture.height / 2);

        FillTexture (_backgroundColour, ref texture);

        while (_isGenerating) {
            StepAndWalk (ref texture, ref point);
            _generatedOutput.texture = texture;
            yield return null;
        }
        Texture2D _finaltexture = new Texture2D (texture.width, texture.height, TextureFormat.RGBA32, true);
        _finaltexture.filterMode = FilterMode.Point;
        FillTexture (_backgroundColour, ref _finaltexture);
        AddPadding (ref texture, ref _finaltexture);
        _generatedOutput.texture = _finaltexture;

    }

    void FillTexture (Color32 fillColour, ref Texture2D texture) {
        for (int w = 0; w < texture.width; w++) {
            for (int h = 0; h < texture.height; h++) {
                texture.SetPixel (w, h, _backgroundColour);
            }
        }
        texture.Apply ();
    }

    void StepAndWalk (ref Texture2D texture, ref Vector2Int point) {
        for (int i = 0; i < _pixelsPerFrame; i++) {
            bool sideStep = Random.Range (0, 2) == 0 ? false : true;

            if (sideStep) {
                point.x += Random.Range (0, 2) == 0 ? -1 : 1;
            } else {
                point.y += Random.Range (0, 2) == 0 ? -1 : 1;
            }

            texture.SetPixel (point.x, point.y, _lineColour);
        }
        texture.Apply ();
    }

    void AddPadding (ref Texture2D texture, ref Texture2D finalTexture) {
        for (int w = 0; w < texture.width; w++) {
            for (int h = 0; h < texture.height; h++) {
                Color pixelColour = texture.GetPixel (w, h);
                if (pixelColour != _backgroundColour) {
                    finalTexture.SetPixel (w, h, _lineColour);

                    for (int p = 0; p < _paddingPixel; p++) {
                        finalTexture.SetPixel (w, h + p, _lineColour);
                        finalTexture.SetPixel (w, h - p, _lineColour);
                        finalTexture.SetPixel (w + p, h, _lineColour);
                        finalTexture.SetPixel (w - p, h, _lineColour);
                    }
                }
            }
        }
        finalTexture.Apply ();
    }
}