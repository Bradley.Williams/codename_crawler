# Dungeon Generator Tool

# Table of Contents:
+ [Overview](#Overview)
+ [Reason For Projects Creation](#Project)
+ [Setup](#Setup)
    * [Step 1](#Step1)
    * [Step 2](#Step2)
    * [Step 3](#Step3)
    * [Step 4](#Step4)
    * [Step 5](#Step5)
    * [Step 6](#Step6)
    * [Step 7](#Step7)
    * [Step 8](#Step8)
    * [Step 9](#Step9)
+ [Known Bugs](#Bugs)
+ [Contact Info](#Info)
+ [State of Project](#State)

## Overview<a name="Overview"></a>
This tool has been made in order to make unique dungeon generation easy for your game. It allows you to create your own rooms and easily implement them into the generator. This generator is easy to implement making sure the user has to do very little in order to get the tool up and running.

## Reason For Projects Creation:<a name="Project"></a>
The project was created as a tool to help people with generating levels with ease. This project is a student project that is a work in progress. There are known bugs in the project as it stands right now.

### Setup:<a name="Setup"></a>
#### Step 1:<a name="Step1"></a>
Create a layer call it whatever you want but keep in mind this is to indicate which portions will not be able to be path found on.![Step1](/uploads/1ff4a77783eeb3bb7f31e55892c6ec95/Step1.gif)
#### Step 2:<a name="Step2"></a>
Import the unity package into your Unity Project.![Step2](/uploads/b6be3d22621013006ec6f66bedc06325/Step2.gif)
#### Step 3:<a name="Step3"></a>
Create an empty object in your hierarchy.![Step3](/uploads/9018c54d5ec0262e203aa518b7cb69f5/Step3.gif)
#### Step 4:<a name="Step4"></a>
Drag the Room spawner, Grid, and Pathfinding script onto it.![Step4](/uploads/c33500ea30515b6cadcd9517d9fe9d8c/Step4.gif)
#### Step 5:<a name="Step5"></a>
For each Room you which to create you will have to create a prefab with the Room script on it.![Step5](/uploads/d3801f5f0049bd8e3192b105354464f6/Step5.gif)
#### Step 6:<a name="Step6"></a>
Fill in all sections for the Room Script. Leave the nearest room blank the roomSpawner script will sort that section out.
#### Step 7:<a name="Step7"></a>
Set all the variables in the Spawner script and the Grid script.![Step7](/uploads/886bd3cb451fcd1cff460d5b74343aec/Step7.gif)
#### Step 8:<a name="Step8"></a>
Hit play![Step8](/uploads/30bfd89b276bf22a8523322bb3291a0b/Step8.gif)
#### Step 9:<a name="Step9"></a>
If you would like to see the formations and the rooms clearer turn the room spawner game object.![Step9](/uploads/54191c2de5594a168f6c7c84b50f4eda/Step9.gif)

## Known Bugs<a name="Bugs"></a>
+ Offset for paths.
+ Paths not spawning for all rooms.
+ Paths using a seeker and destination rather than room points.

## Contact Info<a name="Info"></a>
If there are any major issues please email this contact:
bradley.williams0511@gmail.com

## State of Project<a name="State"></a>
This project is a work in progress and any input or changes from the community are more than welcome.

